PGDMP     .    3                y            OnlineClothingStore    12.9    13.3     -           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            .           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            /           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            0           1262    24591    OnlineClothingStore    DATABASE     r   CREATE DATABASE "OnlineClothingStore" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Russian_Russia.1251';
 %   DROP DATABASE "OnlineClothingStore";
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false            1           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3            �            1259    24653    cart_product    TABLE     z   CREATE TABLE public.cart_product (
    id bigint NOT NULL,
    id_user bigint NOT NULL,
    id_product bigint NOT NULL
);
     DROP TABLE public.cart_product;
       public         heap    postgres    false    3            �            1259    24651    cart_product_id_seq    SEQUENCE     �   ALTER TABLE public.cart_product ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cart_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    213    3            �            1259    24618    category    TABLE     �   CREATE TABLE public.category (
    id bigint NOT NULL,
    title character varying NOT NULL,
    cover character varying NOT NULL,
    "position" bigint NOT NULL
);
    DROP TABLE public.category;
       public         heap    postgres    false    3            �            1259    24616    category_id_seq    SEQUENCE     �   ALTER TABLE public.category ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    205    3            �            1259    24648    favorite_product    TABLE     ~   CREATE TABLE public.favorite_product (
    id bigint NOT NULL,
    id_product bigint NOT NULL,
    id_user bigint NOT NULL
);
 $   DROP TABLE public.favorite_product;
       public         heap    postgres    false    3            �            1259    24646    favorite_product_id_seq    SEQUENCE     �   ALTER TABLE public.favorite_product ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.favorite_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    211    3            �            1259    24640    image_product    TABLE     �   CREATE TABLE public.image_product (
    id bigint NOT NULL,
    id_product bigint NOT NULL,
    cover character varying NOT NULL
);
 !   DROP TABLE public.image_product;
       public         heap    postgres    false    3            �            1259    24638    image_product_id_seq    SEQUENCE     �   ALTER TABLE public.image_product ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.image_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    3    209            �            1259    24626    product    TABLE     �   CREATE TABLE public.product (
    id bigint NOT NULL,
    title character varying NOT NULL,
    cost double precision NOT NULL,
    "desc" character varying NOT NULL,
    category bigint NOT NULL
);
    DROP TABLE public.product;
       public         heap    postgres    false    3            �            1259    24624    product_id_seq    SEQUENCE     �   ALTER TABLE public.product ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    3    207            �            1259    24610    users    TABLE     �   CREATE TABLE public.users (
    id bigint NOT NULL,
    token character varying NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL
);
    DROP TABLE public.users;
       public         heap    postgres    false    3            �            1259    24608    users_id_seq    SEQUENCE     �   ALTER TABLE public.users ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    203    3            *          0    24653    cart_product 
   TABLE DATA           ?   COPY public.cart_product (id, id_user, id_product) FROM stdin;
    public          postgres    false    213   �       "          0    24618    category 
   TABLE DATA           @   COPY public.category (id, title, cover, "position") FROM stdin;
    public          postgres    false    205   �       (          0    24648    favorite_product 
   TABLE DATA           C   COPY public.favorite_product (id, id_product, id_user) FROM stdin;
    public          postgres    false    211   �       &          0    24640    image_product 
   TABLE DATA           >   COPY public.image_product (id, id_product, cover) FROM stdin;
    public          postgres    false    209   �       $          0    24626    product 
   TABLE DATA           D   COPY public.product (id, title, cost, "desc", category) FROM stdin;
    public          postgres    false    207   �!                  0    24610    users 
   TABLE DATA           ;   COPY public.users (id, token, email, password) FROM stdin;
    public          postgres    false    203   �(       2           0    0    cart_product_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cart_product_id_seq', 13, true);
          public          postgres    false    212            3           0    0    category_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.category_id_seq', 6, true);
          public          postgres    false    204            4           0    0    favorite_product_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.favorite_product_id_seq', 18, true);
          public          postgres    false    210            5           0    0    image_product_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.image_product_id_seq', 40, true);
          public          postgres    false    208            6           0    0    product_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.product_id_seq', 15, true);
          public          postgres    false    206            7           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 11, true);
          public          postgres    false    202            *   ,   x���4�44�24 і\���@d�eh�͹�A��W� �D)      "   �   x����j1��s�0;�l�f{�((�VPA�
jYE�h�O�R�j�U�U摌{�e�4?�c���7�s)���x�
�E��;�&s{N��wR�CZ��Jm�Q��e�"M��p}�{B�X�/|�Gdg��K���<'LG�wql�����k>?�B�5���P&-�HǨma�)�X:Rv|��?4o߂fQyė��P�#)�	�h�      (   '   x�34�4�4�24�4Q朆&���\��F :F��� \��      &   �  x���Mo1��c��e{�-�T
jD�-JR!Y�DQ���� �s�����=�z=^t��麏�o�>_���}y� �B�w�_c�����}FA��1��L�˯�w2;��6;��r��s��ח����=3��f.ovR3W�:�����,������v�v@�=�»�Tbe�: u~�U���AB�$���̐6I#������32aB��6+~�礈Y��P'�;��N�`��Y�Y�*�hV��H�
���$�f_*a�Q�DQ�]�`��)1�!iJL�ʅ�ߜtxQ$ϥ:g��R��I�7K��]j�
�lę����]�#z�S�Kg9�]:�.9�zV��(<ܓBu�o��v���
�T��*5�z+
7��E�I=��l�f���y�Ӕv�K��W�a�Ŧ�쒶�g�.�����6]i�ԓ_K��Ͷ�4��D�Hv���>,���3v      $   �  x��W�NW}�yAjU��
8o$!	��8IS�e�8��Ș�}RR���iU5m��T26��`���/�K���g�m� xΜ�/k������5���ЎzX���|њ��E-��n�{���hG7��Mz��u��=z��;��?�=�Pwx��+���ڃ	�]����JyÊ'f�����څ�]���{�+���n�~�ҿ�>v�Ǻ���ҺZ�إ/V2���?�-�7a��O��ԴJE��J�(�`�\7������º�J<:e�B�(�j�S��𿧲卲Z�7�����])��xt.z��Y6�;�]p�]�='X�� �b���jL\�,��`��t{���ܵ�S�I�&S� n鿀��>��\�[�:���U|QP�ەbɶR��1�htJ�q��}��d*	���)��+��S�x*������
�]�L�w�w����|����j���"
�Pbܑb��a��sMdA���-m�.9���`���_�63�<�0���0�30�lQ�"m�^��XMG��\� ��{z �Q�(`@�9��W�w@�Op��h:�~�k\r�E\�!^�aڣG��}�����ÄU��[_���Cꍈ5B��⹅@�juQ�̨[+��ʲ�,ܽ��|�I��P���t���/SR���SV"����N	�r0M#���cu��Z\��X�ϭ�g�����y�L0�a����G�J��\�S��L�~���N���&�=�Q��)�v@���e��j`ɸ��2��=,|C���DT1�֥h:\G�cK���}&
L�9k��� N$ͤ�gbJu�Z.���Jq��b��%����3
�[+Q�s��g��(L�B^�:Z��NOԷ�=['`��[�ǃđ�[S��˞Fʮ�
��&3O�w���`Z����e��>��!�j�9oR�m��:�NƯE��Ey�7|����9�|�Lum�.*tr�xH�5=BAc�1R\b�*+	C�N	H53
m0	������ʎ7��뱪5��SF͑�G�H�O0�*A���\y>c��AC�0��1�oV�a#�;3e��ͫ�����J���&IB~���
m�e�ת�Z*JU{{K0��8�C3���A�"�#��^"���$}��<�3��5y�Q�r�F����HJwf���Dš���h�8�U��������(hIf|�c���+�s��Kz�#n:��ˎE��rG�&0=��}��1ʱ�c����0^���IS�'3����p��orPu?,�jj��L�xJ�u�;a���!HOD�zW���]�ƀ���A=:�o,@sX���o��6�+m�\�k.L4�6��O���~��Ҕft��Qٮ!.���c�)��,����qI���WH9<b�R�� �1g��Ӓ��ҚL��'O�ժ����^�S�����Zx����z�j��ij�?�ݳ�L�Ahp��9ְ��
rM6����h%Ϲ�V����Rq:Wȯ]ޟ^_�۞2���;QC�r;V,��Q�Xg�5D�M3:���҄C�W��j��l������\��l��y%�#N�z(�QZآQ�^��$B��w��द��#cQ��
�M�bW˕p ��U�2
��>��tc� �:�S�'_~L���`6�q���C�Ҡ��4�&�I��h��J�Q�H\t�AJz�1���zq����\݄�ё0q�A%G,е9�%���I<�Q~�U�J_wG�/G�}(5�����Ӣ3w�a����Ѝz.:�M�GgB_DB����է          (   x�34䬨�Э@&8KR�K�s3s���s�\�=... �]~     