from flask import jsonify, request, url_for, g, abort, app
import pandas as pd
import random
from . import bp
from ..api.errors import *
from ..api.utils import *
from ..api.request_to_db import *
from ..api.converters import *


@bp.route('/login', methods=['POST'])
@required_params(params={"email": "param", "password": "param"})
def login():
    email = request.args.get("email")
    password = request.args.get("password")
    new_token = create_new_token()

    user = get_user(email, password)
    if user.empty:
        return error_response_model("user not found", 404)

    set_new_token(new_token, email, password)
    answer = convert_and_clear_user(user, new_token=new_token)
    return jsonify(add_info_response(answer))


@bp.route('/signup', methods=['POST'])
@required_params(params={"email": "body", "password": "body"})
def signup():
    body = request.get_json()

    email = body["email"]
    password = body["password"]
    new_token = create_new_token()

    if is_exist_user_with_email(email):
        return error_response_model("user with this email already exist")

    insert_new_user(email, password, new_token)
    user = get_user(email, password)

    if user.empty:
        return error_response_model("create user not found", 404)

    answer = convert_and_clear_user(user, new_token=new_token)
    return jsonify(add_info_response(answer))


@bp.route('/category', methods=['GET'])
@required_params(params={"token": "param"})
@check_token()
def all_category():
    return jsonify(add_info_response(convert_results_to_list_dict(get_all_category())))


@bp.route('/product_category', methods=['GET'])
@required_params(params={"token": "param", "id_category": "param"})
@check_token()
def get_product_category():
    token = request.args.get("token")
    id_category = request.args.get("id_category")
    products = convert_results_to_list_dict(get_products(id_category))

    if not is_exist_category_with_id(id_category):
        return error_response_model("category with this id not exist")

    for index, product in enumerate(products):
        id_product = product["id"]
        covers_product = [image["cover"] for image in convert_results_to_list_dict(get_image_product(id_product))]
        products[index]["images"] = covers_product
    products = add_cart_favorite_to_products(token, products)
    return jsonify(add_info_response(products))


@bp.route('/favorite_product', methods=['GET'])
@required_params(params={"token": "param"})
@check_token()
def get_favorites():
    token = request.args.get("token")
    user_id = convert_results_to_dict(get_user_from_token(token))["id"]
    favorite_products = convert_results_to_list_dict(get_favorite(user_id))
    products = [convert_results_to_dict(get_product(product["id_product"])) for product in favorite_products]
    products = add_cart_favorite_to_products(token, products)
    return jsonify(add_info_response(products))


@bp.route('/cart_product', methods=['GET'])
@required_params(params={"token": "param"})
@check_token()
def get_user_cart():
    token = request.args.get("token")
    user_id = convert_results_to_dict(get_user_from_token(token))["id"]
    cart_product = convert_results_to_list_dict(get_cart(user_id))
    products = [convert_results_to_dict(get_product(product["id_product"])) for product in cart_product]
    products = add_cart_favorite_to_products(token, products)
    return jsonify(add_info_response(products))


@bp.route('/set_favorite', methods=['POST'])
@required_params(params={"token": "param", "id_product": "param", "is_favorite": "param"})
@check_token()
def set_favorite():
    token = request.args.get("token")
    id_user = convert_results_to_dict(get_user_from_token(token))["id"]
    id_product = request.args.get("id_product")
    is_favorite = parseBooleanParam(request.args.get("is_favorite", type=str))

    if is_favorite is None:
        return error_response_model("param is_favorite not correct")
    if not is_exist_product_with_id(id_product):
        return error_response_model("product with this id not exist")

    set_favorite_product(is_favorite, id_user, id_product)
    favorite_product = convert_results_to_dict(get_product(id_product))
    favorite_product = add_cart_favorite_to_product(token, favorite_product)
    return jsonify(add_info_response(favorite_product))


@bp.route('/set_cart', methods=['POST'])
@required_params(params={"token": "param", "id_product": "param", "is_cart": "param"})
@check_token()
def set_cart():
    token = request.args.get("token")
    id_user = convert_results_to_dict(get_user_from_token(token))["id"]
    id_product = request.args.get("id_product")
    is_cart = parseBooleanParam(request.args.get("is_cart", type=str))

    if is_cart is None:
        return error_response_model("param is_cart not correct")
    if not is_exist_product_with_id(id_product):
        return error_response_model("product with this id not exist")

    set_cart_product(is_cart, id_user, id_product)
    cart_product = convert_results_to_dict(get_product(id_product))
    cart_product = add_cart_favorite_to_product(token, cart_product)
    return jsonify(add_info_response(cart_product))

