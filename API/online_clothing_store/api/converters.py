def convert_dict_to_str(dict_):
    str_dict = str(dict_).replace("'", "\"")
    return str_dict


def convert_results_to_dict(result):
    return [i.to_dict() for _, i in result.iterrows()][0]


def convert_results_to_list_dict(result):
    return [i.to_dict() for _, i in result.iterrows()]