import pandas as pd
from ..api.utils import *
from ..api import conn
from ..api.converters import *


def get_user(email: str, password: str):
    with conn.begin() as tran:
        try:
            user = pd.read_sql(
                f'SELECT * FROM public.users WHERE email=\'{email}\' AND "password"=\'{password}\';',
                conn)
        except:
            tran.rollback()
            raise

    return user


def is_exist_user_with_email(email: str):
    with conn.begin() as tran:
        try:
            users = pd.read_sql(
                f'SELECT * FROM public.users WHERE email=\'{email}\';',
                conn)
        except:
            tran.rollback()
            raise
    answers = convert_results_to_list_dict(users)
    return len(answers)


def is_exist_product_with_id(id_product: str):
    with conn.begin() as tran:
        try:
            product = pd.read_sql(
                f'SELECT * FROM public.product WHERE id=\'{id_product}\';',
                conn)
        except:
            tran.rollback()
            raise
    return not product.empty


def is_exist_category_with_id(id_category: str):
    with conn.begin() as tran:
        try:
            category = pd.read_sql(
                f'SELECT * FROM public.category WHERE id=\'{id_category}\';',
                conn)
        except:
            tran.rollback()
            raise
    return not category.empty


def is_favorite_product_token(id_product: int, token: str):
    user_id = convert_results_to_dict(get_user_from_token(token))["id"]
    favorite_products_user = convert_results_to_list_dict(get_favorite(user_id))
    ids_favorite_products = [i["id_product"] for i in favorite_products_user]
    return id_product in ids_favorite_products


def is_cart_product_token(id_product: int, token: str):
    user_id = convert_results_to_dict(get_user_from_token(token))["id"]
    cart_products_user = convert_results_to_list_dict(get_cart(user_id))
    ids_cart_products = [i["id_product"] for i in cart_products_user]
    return id_product in ids_cart_products


def insert_new_user(email: str, password: str, token: str):
    with conn.begin() as tran:
        try:
            conn.execute(f"INSERT INTO public.users (\"token\", email, \"password\") VALUES ('{token}', "
                         f"'{email}', '{password}');")
        except:
            tran.rollback()
            raise


def set_new_token(new_token: str, email: str, password: str):
    with conn.begin() as tran:
        try:
            conn.execute(f'update public.users set "token"=\'{new_token}\' where "email"=\'{email}\' and '
                         f'"password"=\'{password}\';')
        except:
            tran.rollback()
            raise


def insert_new_favorite_product(id_user: int, id_product: int):
    with conn.begin() as tran:
        try:
            conn.execute(f"INSERT INTO public.favorite_product (id_product, id_user) VALUES ('{id_product}', '{id_user}');")
        except:
            tran.rollback()
            raise


def delete_old_favorite_product(id_user: int, id_product: int):
    with conn.begin() as tran:
        try:
            conn.execute(f"DELETE FROM public.favorite_product WHERE id_product=\'{id_product}\' AND id_user=\'{id_user}\';")
        except:
            tran.rollback()
            raise


def insert_new_cart_product(id_user: int, id_product: int):
    with conn.begin() as tran:
        try:
            conn.execute(f"INSERT INTO public.cart_product (id_product, id_user) VALUES ('{id_product}', '{id_user}');")
        except:
            tran.rollback()
            raise


def delete_old_cart_product(id_user: int, id_product: int):
    with conn.begin() as tran:
        try:
            conn.execute(f"DELETE FROM public.cart_product WHERE id_product=\'{id_product}\' AND id_user=\'{id_user}\';")
        except:
            tran.rollback()
            raise


def set_favorite_product(isFavorite: bool, id_user: int, id_product: int):
    if isFavorite:
        insert_new_favorite_product(id_user, id_product)
    else:
        delete_old_favorite_product(id_user, id_product)


def set_cart_product(isCart: bool, id_user: int, id_product: int):
    if isCart:
        insert_new_cart_product(id_user, id_product)
    else:
        delete_old_cart_product(id_user, id_product)


def get_all_category():
    with conn.begin() as tran:
        try:
            category = pd.read_sql(f'select * from public.category;', conn)
        except:
            tran.rollback()
            raise
    return category


def get_user_from_token(token: str):
    with conn.begin() as tran:
        try:
            user = pd.read_sql(f'select * from public.users where "token"=\'{token}\';', conn)
        except:
            tran.rollback()
            raise

    return user


def convert_and_clear_user(user, clear_password=True, clear_token=False, new_token:str=None):
    clear_user = convert_results_to_dict(user)

    if new_token is not None:
        clear_user["token"] = new_token

    if clear_token and "token" in clear_user:
        del clear_user["token"]

    if clear_password and "password" in clear_user:
        del clear_user["password"]

    return clear_user


def check_available_token(token):
    user_from_token = get_user_from_token(token)
    return not user_from_token.empty


def get_image_product(id_product: int):
    with conn.begin() as tran:
        try:
            images = pd.read_sql(f'select * from public.image_product where "id_product"=\'{id_product}\';', conn)
        except:
            tran.rollback()
            raise
    return images


def get_products(id_category: int):
    with conn.begin() as tran:
        try:
            products = pd.read_sql(f'select * from public.product where "category"=\'{id_category}\';', conn)
        except:
            tran.rollback()
            raise
    return products


def get_product(id_product: int):
    with conn.begin() as tran:
        try:
            product = pd.read_sql(f'select * from public.product where "id"=\'{id_product}\';', conn)
        except:
            tran.rollback()
            raise
    return product


def get_products_with_full_info(title: str, cost: float, desc: str, category: int):
    with conn.begin() as tran:
        try:
            products = pd.read_sql(f"SELECT * FROM public.product WHERE (title, \"cost\", \"desc\", category) "
                                  f"VALUES ('{title}', '{cost}', '{desc}', '{category}');", conn)
        except:
            tran.rollback()
            raise
    return products


def get_favorite(id_user: int):
    with conn.begin() as tran:
        try:
            favorites = pd.read_sql(f'select * from public.favorite_product where "id_user"=\'{id_user}\';', conn)
        except:
            tran.rollback()
            raise
    return favorites


def get_cart(id_user: int):
    with conn.begin() as tran:
        try:
            cart = pd.read_sql(f'select * from public.cart_product where "id_user"=\'{id_user}\';', conn)
        except:
            tran.rollback()
            raise
    return cart


def insert_new_product(title, cost, desc, category):
    with conn.begin() as tran:
        try:
            conn.execute(f"INSERT INTO public.product (title, \"cost\", \"desc\", category) VALUES ('{title}', "
                         f"'{cost}', '{desc}', '{category}');")
        except:
            tran.rollback()
            raise


def insert_images_product(id_product, img_path):
    with conn.begin() as tran:
        try:
            conn.execute(f"INSERT INTO public.image_product (id_product, cover) VALUES ('{id_product}', '{img_path}');")
        except:
            tran.rollback()
            raise


def insert_favorite_product(id_product, id_user):
    with conn.begin() as tran:
        try:
            conn.execute(f"INSERT INTO public.favorite_product (id_product, id_user) "
                         f"VALUES ('{id_product}', '{id_user}');")
        except:
            tran.rollback()
            raise


def insert_cart_product(id_product, id_user):
    with conn.begin() as tran:
        try:
            conn.execute(f"INSERT INTO public.cart_product (id_product, id_user) "
                         f"VALUES ('{id_product}', '{id_user}');")
        except:
            tran.rollback()
            raise