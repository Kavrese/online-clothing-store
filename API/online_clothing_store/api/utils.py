from flask import request
from online_clothing_store.api.errors import error_response
from ..api.errors import *
import random
from ..api.request_to_db import *


def check_params(params, request_):
    for i in params:
        if request_.args.get(i, type=str) is None:
            return error_response(404, message=f"{i} not found in params request")
    return None


def create_new_token():
    mask = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx"
    variants_x = "abcdefghijklmnopqrstuvwxyz0123456789"
    return "".join([random.choice(variants_x) if (i != "-") else i for i in mask])


def add_info_response(data, success=True):
    payload = {'data': data, "success": success}
    return payload


# Проверка параметров (новая) - декоратор
# params - список обязательных параметров или данных в теле запроса
def required_params(params: dict):
    def inner(func):
        def wrapper(*args, **kwargs):

            check_body = not all([params[i] == "param" for i in params])
            check_param = not all([params[i] == "body" for i in params])

            if check_body and request.form is None:
                return error_response_model(f'body not found in request', 201)

            if check_param:
                need_params = [i for i in params if params[i] == "param"]
                no_exist_params = [param for param in need_params if param not in request.args]
                if len(no_exist_params):
                    return error_response_model(f"{', '.join(no_exist_params)} not found in request params", 404)
            if check_body:
                need_body_params = [i for i in params if params[i] == "body"]
                no_exist_body_params = [param for param in need_body_params if param not in request.get_json()]
                if len(no_exist_body_params):
                    return error_response_model(f"{', '.join(no_exist_body_params)} not found in request body params",
                                                201)

            return func(*args, **kwargs)

        wrapper.__name__ = func.__name__
        return wrapper

    return inner


# Проверка токена
def check_token():
    def inner(func):
        def wrapper(*args, **kwargs):
            token = request.args.get("token")
            if not check_available_token(token):
                return error_response_model(f"token not available", 202)
            return func(*args, **kwargs)

        wrapper.__name__ = func.__name__
        return wrapper

    return inner


def parseBooleanParam(value: str):
    variants = {"true": True, "false": False, "True": True, "False": False}
    return variants[value] if value in variants else None


def add_cart_favorite_to_product(token: str, product: dict):
    product["isFavorite"] = is_favorite_product_token(product["id"], token)
    product["isCart"] = is_cart_product_token(product["id"], token)
    return product


def add_cart_favorite_to_products(token: str, products: list):
    for index, product in enumerate(products):
        products[index] = add_cart_favorite_to_product(token, product)
    return products
