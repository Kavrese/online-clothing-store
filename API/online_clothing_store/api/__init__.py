from flask import Blueprint
from sqlalchemy import create_engine

HOST_DB = "localhost"
PORT_DP = "5432"
NAME_DB = "OnlineClothingStore"
USER_DB = "postgres"
PASSWORD_DB = "QwE123asd"

conn = create_engine(f'postgresql+psycopg2://{USER_DB}:{PASSWORD_DB}@{HOST_DB}:{PORT_DP}/{NAME_DB}').connect()
bp = Blueprint('api', __name__)


from . import api
