from flask_sqlalchemy import SQLAlchemy
# from flask_migrate import Migrate
from flask_login import LoginManager
# from flask_mail import Mail

from flask_bootstrap import Bootstrap
from flask_moment import Moment
# from redis import Redis
# import rq
from . import terminal

moment = Moment()

app = terminal.create_app(moment=moment)