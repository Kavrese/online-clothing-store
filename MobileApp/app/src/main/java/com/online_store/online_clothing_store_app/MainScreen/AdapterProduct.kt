package com.online_store.online_clothing_store_app.MainScreen

import android.app.Activity
import android.content.Context
import android.view.ActionMode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.online_store.online_clothing_store_app.R
import com.online_store.online_clothing_store_app.common.*
import com.online_store.online_clothing_store_app.models.AnswerModelProduct
import com.online_store.online_clothing_store_app.models.ModelProduct
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_product.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdapterProduct(private val activity: Activity, private val products: MutableList<ModelProduct> = arrayListOf()):
    RecyclerView.Adapter<AdapterProduct.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(holder.itemView.context)
            .load(products[position].images[0])
            .into(holder.itemView.cover_product)

        holder.itemView.name_product.text = products[position].title
        holder.itemView.desc_product.text = products[position].desc
        holder.itemView.favorite.setImageResource(if (products[position].isFavorite) R.drawable.ic_baseline_favorite_24 else R.drawable.ic_baseline_favorite_border_24)
        holder.itemView.cost_product.text = addSpacesToStrFloat(products[position].cost.toInt().toString()) + "₽"

        holder.itemView.favorite.setOnClickListener {
            set_favorite_product(holder.itemView.context, activity, position, !products[position].isFavorite, object: OnFavorite{
                override fun onResult(modelProduct: ModelProduct) {
                    products[holder.adapterPosition] = modelProduct
                    holder.itemView.favorite.setImageResource(if (products[holder.adapterPosition].isFavorite) R.drawable.ic_baseline_favorite_24 else R.drawable.ic_baseline_favorite_border_24)
                }
            })
        }
    }

    override fun getItemCount(): Int = products.size

    fun setData(data: MutableList<ModelProduct>){
        products.clear()
        products.addAll(data)
        notifyDataSetChanged()
    }

    private fun addSpacesToStrFloat(num: String): String{
        if (num.length <= 3)
            return num

        val numSpaces = mutableListOf<String>()
        for (i in num.reversed().indices){
            if (i != 0 && i % 3 == 0)
                numSpaces.add(" ")
            numSpaces.add(num.reversed()[i].toString())
        }
        numSpaces.reverse()

        return numSpaces.joinToString (separator = "")
    }

    private fun set_favorite_product(context: Context, activity: Activity, index_product: Int, isFavorite: Boolean, onFavorite: OnFavorite){
        val id_product = products[index_product].id.toString()
        initRetrofit().set_favorite(isFavorite, id_product, Saver.token!!).enqueue(object: Callback<AnswerModelProduct>{
            override fun onResponse(
                call: Call<AnswerModelProduct>,
                response: Response<AnswerModelProduct>
            ) {
                if (!checkNotAvailableTokenAndStartAuthScreen(activity, response.code())){
                    if (response.body() != null){
                        if (response.body()!!.success){
                            onFavorite.onResult(response.body()!!.data)
                        }else{
                            showDialogAlert(context, "Ошибка", response.body()!!.error)
                        }
                    }else{
                        showDialogAlert(context, "Ошибка")
                    }
                }
            }

            override fun onFailure(call: Call<AnswerModelProduct>, t: Throwable) {
                showDialogAlert(context, "Ошибка", t)
            }
        })
    }
}