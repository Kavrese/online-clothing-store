package com.online_store.online_clothing_store_app.common

import com.online_store.online_clothing_store_app.models.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {
    @POST("login")
    fun login(@Query("email") email: String, @Query("password") password: String): Call<AnswerModelUser>

    @POST("signup")
    fun signup(@Body bodySignUp: BodySignUp): Call<AnswerModelUser>

    @GET("category")
    fun category(@Query("token") token: String): Call<AnswerModelCategory>

    @GET("product_category")
    fun product_category(@Query("token") token: String, @Query("id_category") id_category: Int): Call<AnswerModelListModelProduct>

    @GET("cart_product")
    fun cart_product(@Query("token") token: String): Call<AnswerModelListModelProduct>

    @GET("favorite_product")
    fun favorite_product(@Query("token") token: String): Call<AnswerModelListModelProduct>

    @POST("set_favorite")
    fun set_favorite(@Query("is_favorite") is_favorite: Boolean, @Query("id_product") id_product: String, @Query("token") token: String): Call<AnswerModelProduct>

    @POST("set_cart")
    fun set_cart(@Query("is_cart") is_cart: Boolean, @Query("id_product") id_product: String, @Query("token") token: String): Call<AnswerModelProduct>
}