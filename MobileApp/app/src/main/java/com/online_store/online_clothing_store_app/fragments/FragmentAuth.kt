package com.online_store.online_clothing_store_app.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.online_store.online_clothing_store_app.CategoryScreen.CategoryActivity
import com.online_store.online_clothing_store_app.R
import com.online_store.online_clothing_store_app.common.*
import com.online_store.online_clothing_store_app.models.ModelUser
import kotlinx.android.synthetic.main.fragment_auth.*

class FragmentAuth: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_auth, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        auth.setOnClickListener {
            val email = email_auth.text.toString()
            val password = password_auth.text.toString()
            if (email.isNotEmpty() && password.isNotEmpty()){
                if (validateEmail(email)){
                    auth(requireContext(), email, password, object: OnCompleteRequestIdentification{
                        override fun result(modelModelUser: ModelUser) {
                            Saver.token = modelModelUser.token
                            saveToken(requireContext(), Saver.token!!)
                            startActivity(Intent(requireContext(), CategoryActivity::class.java))
                            requireActivity().finish()
                        }
                    })
                }else{
                    showDialogAlert(requireContext(), "Ошибка авторизации", "Почта не корректна")
                }
            }else{
                showDialogAlert(requireContext(), "Ошибка авторизации")
            }
        }
    }
}