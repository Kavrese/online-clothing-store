package com.online_store.online_clothing_store_app.common

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import com.online_store.online_clothing_store_app.IdentificationScreen.IdentificationActivity
import com.online_store.online_clothing_store_app.models.AnswerModelUser
import com.online_store.online_clothing_store_app.models.BodySignUp
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun initRetrofit(): Api{
    return Retrofit.Builder()
        .baseUrl("http://192.168.1.66:4000/api/")
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(Api::class.java)
}

fun showDialogAlert(context: Context, title: String, message: String){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(message)
        .setNegativeButton("ok", null)
        .show()
}

fun showDialogAlert(context: Context, title: String, t: Throwable){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(t.message)
        .setNegativeButton("ok", null)
        .show()
}

fun showDialogAlert(context: Context, title: String){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage("Повторите попытку позже !")
        .setNegativeButton("ok", null)
        .show()
}

fun validateEmail(email: String): Boolean{
    return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun saveToken(context: Context, token: String?){
    val sh = context.getSharedPreferences("0", 0)
    sh.edit()
        .putString("token", token)
        .apply()
}

fun loadToken(context: Context): String? {
    val sh = context.getSharedPreferences("0", 0)
    return sh.getString("token", null)
}

fun parseIdentification(context: Context, title_alert_dialog: String, answer: AnswerModelUser?, onCompleteRequestIdentification: OnCompleteRequestIdentification){
    if (answer != null){
        if (answer.error == null){
            setFirstEnter(context)
            onCompleteRequestIdentification.result(answer.data)
        }else{
            showDialogAlert(context, title_alert_dialog, answer.error)
        }
    }else{
        showDialogAlert(context, title_alert_dialog)
    }
}

fun auth(context: Context, email: String, password: String, onCompleteRequestIdentification: OnCompleteRequestIdentification) {
    val title = "Ошибка авторизации"
    initRetrofit().login(email, password).enqueue(object: Callback<AnswerModelUser>{
        override fun onResponse(call: Call<AnswerModelUser>, response: Response<AnswerModelUser>) {
            parseIdentification(context, title, response.body(), onCompleteRequestIdentification)
        }

        override fun onFailure(call: Call<AnswerModelUser>, t: Throwable) {
            showDialogAlert(context, title, t)
        }
    })
}

fun reg(context: Context, bodySignUp: BodySignUp, onCompleteRequestIdentification: OnCompleteRequestIdentification){
    val title = "Ошибка регистрации"
    initRetrofit().signup(bodySignUp).enqueue(object: Callback<AnswerModelUser>{
        override fun onResponse(call: Call<AnswerModelUser>, response: Response<AnswerModelUser>) {
            parseIdentification(context, title, response.body(), onCompleteRequestIdentification)
        }

        override fun onFailure(call: Call<AnswerModelUser>, t: Throwable) {
            showDialogAlert(context, title, t)
        }
    })
}

fun checkNotAvailableTokenAndStartAuthScreen(activity: Activity, status_code: Int, code_token_error: Int = 202): Boolean{
    if (status_code == code_token_error) {
        Saver.token = null
        saveToken(activity, null)
        val intent = Intent(activity, IdentificationActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        activity.startActivity(intent)
        activity.finish()
        return true
    }
    return false
}

fun setFirstEnter(context: Context){
    val sh = context.getSharedPreferences("0", 0)
    sh.edit()
        .putBoolean("isBeforeFirst", true)
        .apply()
}

fun isBeforeEnter(context: Context): Boolean{
    val sh = context.getSharedPreferences("0", 0)
    return sh.getBoolean("isBeforeFirst", false)
}