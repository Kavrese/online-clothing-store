package com.online_store.online_clothing_store_app.IdentificationScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.online_store.online_clothing_store_app.CategoryScreen.CategoryActivity
import com.online_store.online_clothing_store_app.R
import com.online_store.online_clothing_store_app.common.Saver
import com.online_store.online_clothing_store_app.common.isBeforeEnter
import com.online_store.online_clothing_store_app.common.loadToken
import kotlinx.android.synthetic.main.activity_identification.*

class IdentificationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_identification)

        toNextScreenIfExistToken()
        initNavigation()

        toAuthFragmentIfBeforeEnter()
    }

    private fun initNavigation(){
        tab_layout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                pager2.setCurrentItem(tab!!.position, true)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }
        })

        pager2.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                tab_layout.selectTab(tab_layout.getTabAt(position)!!)
            }
        })

        pager2.adapter = AdapterIdentificationFragment(this)
    }

    private fun toNextScreenIfExistToken(){
        Saver.token = loadToken(this)
        if (Saver.token != null){
            startActivity(Intent(this, CategoryActivity::class.java))
            finish()
        }
    }

    private fun toAuthFragmentIfBeforeEnter(){
        if (isBeforeEnter(this))
            pager2.setCurrentItem(1, false)
    }
}