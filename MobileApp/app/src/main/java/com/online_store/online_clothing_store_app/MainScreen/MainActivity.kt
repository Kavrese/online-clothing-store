package com.online_store.online_clothing_store_app.MainScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.online_store.online_clothing_store_app.R
import com.online_store.online_clothing_store_app.common.Saver
import com.online_store.online_clothing_store_app.common.checkNotAvailableTokenAndStartAuthScreen
import com.online_store.online_clothing_store_app.common.initRetrofit
import com.online_store.online_clothing_store_app.common.showDialogAlert
import com.online_store.online_clothing_store_app.models.AnswerModelCategory
import com.online_store.online_clothing_store_app.models.AnswerModelListModelProduct
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        title_product.text = Saver.select_category.title

        rec_products.apply{
            adapter = AdapterProduct(this@MainActivity)
            layoutManager = GridLayoutManager(this@MainActivity, 2)
        }

        back.setOnClickListener {
            finish()
        }

        updateDataProducts()
    }

    private fun updateDataProducts(){
        initRetrofit().product_category(Saver.token!!, Saver.select_category.id).enqueue(object:
            Callback<AnswerModelListModelProduct>{
            override fun onResponse(
                call: Call<AnswerModelListModelProduct>,
                response: Response<AnswerModelListModelProduct>
            ) {
                if (!checkNotAvailableTokenAndStartAuthScreen(this@MainActivity, response.code())) {
                    if (response.body() != null) {
                        if (response.body()!!.success) {
                            (rec_products.adapter as AdapterProduct).setData(response.body()!!.data.toMutableList())
                        } else {
                            showDialogAlert(
                                this@MainActivity,
                                "Ошибка получения продуктов категории",
                                response.body()!!.error
                            )
                        }
                    } else {
                        showDialogAlert(this@MainActivity, "Ошибка получения продуктов категории")
                    }
                }
            }

            override fun onFailure(call: Call<AnswerModelListModelProduct>, t: Throwable) {
                showDialogAlert(this@MainActivity, "Ошибка получения продуктов категории", t)
            }

        })
    }
}