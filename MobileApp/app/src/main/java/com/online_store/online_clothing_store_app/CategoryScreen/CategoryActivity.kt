package com.online_store.online_clothing_store_app.CategoryScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.online_store.online_clothing_store_app.MainScreen.MainActivity
import com.online_store.online_clothing_store_app.R
import com.online_store.online_clothing_store_app.common.*
import com.online_store.online_clothing_store_app.models.AnswerModelCategory
import com.online_store.online_clothing_store_app.models.ModelCategory
import kotlinx.android.synthetic.main.activity_category.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CategoryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)

        rec_category.apply {
            adapter = AdapterCategory(onClickCategory = object: OnClickCategory{
                override fun onClick(modelCategory: ModelCategory) {
                    Saver.select_category = modelCategory
                    startActivity(Intent(this@CategoryActivity, MainActivity::class.java))
                }
            })
            layoutManager = LinearLayoutManager(this@CategoryActivity)
        }

        updateDataCategory()
    }

    private fun updateDataCategory(){
        initRetrofit().category(Saver.token!!).enqueue(object: Callback<AnswerModelCategory>{
            override fun onResponse(
                call: Call<AnswerModelCategory>,
                response: Response<AnswerModelCategory>
            ) {
                if (!checkNotAvailableTokenAndStartAuthScreen(this@CategoryActivity, response.code())) {
                    if (response.body() != null) {
                        if (response.body()!!.success) {
                            val data = response.body()!!.data.toMutableList()
                            data.sortedBy { it.position }
                            (rec_category.adapter as AdapterCategory).setData(data)
                        }else {
                            showDialogAlert(
                                this@CategoryActivity,
                                "Ошибка получения категорий",
                                response.body()!!.error
                            )
                        }
                    } else {
                        showDialogAlert(this@CategoryActivity, "Ошибка получения категорий")
                    }
                }
            }

            override fun onFailure(call: Call<AnswerModelCategory>, t: Throwable) {
                showDialogAlert(this@CategoryActivity, "Ошибка получения категорий", t)
            }
        })
    }
}