package com.online_store.online_clothing_store_app.IdentificationScreen

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.online_store.online_clothing_store_app.fragments.FragmentAuth
import com.online_store.online_clothing_store_app.fragments.FragmentReg

class AdapterIdentificationFragment(fa: FragmentActivity): FragmentStateAdapter(fa) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return arrayListOf(FragmentReg(), FragmentAuth())[position]
    }
}