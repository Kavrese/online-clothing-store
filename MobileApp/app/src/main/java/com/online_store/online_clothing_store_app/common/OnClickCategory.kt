package com.online_store.online_clothing_store_app.common

import com.online_store.online_clothing_store_app.models.ModelCategory

interface OnClickCategory {
    fun onClick(modelCategory: ModelCategory)
}