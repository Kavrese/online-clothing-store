package com.online_store.online_clothing_store_app.models

data class ModelUser(
    val id: Int,
    val email: String,
    val token: String?
)

data class BodySignUp(
    val email: String,
    val password: String
)

data class AnswerModelUser(
    val success: Boolean,
    val error: String?,
    val data: ModelUser
)

data class AnswerModelProduct(
    val success: Boolean,
    val error: String,
    val data: ModelProduct
)

data class AnswerModelCategory(
    val success: Boolean,
    val error: String,
    val data: List<ModelCategory>
)

data class ModelProduct(
    val title: String,
    val cost: Float,
    val category: Int,
    val desc: String,
    val images: List<String>,
    val isFavorite: Boolean,
    val isCart: Boolean,
    val id: Int
)

data class ModelCategory(
    val title: String,
    val cover: String,
    val id: Int,
    val position: Int
)

data class AnswerModelListModelProduct(
    val success: Boolean,
    val error: String,
    val data: List<ModelProduct>
)