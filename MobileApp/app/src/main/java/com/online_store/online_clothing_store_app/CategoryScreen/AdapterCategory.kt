package com.online_store.online_clothing_store_app.CategoryScreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.online_store.online_clothing_store_app.R
import com.online_store.online_clothing_store_app.common.OnClickCategory
import com.online_store.online_clothing_store_app.models.ModelCategory
import kotlinx.android.synthetic.main.item_category.view.*

class AdapterCategory(private val categores: MutableList<ModelCategory> = arrayListOf(), private val onClickCategory: OnClickCategory? = null):
    RecyclerView.Adapter<AdapterCategory.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener{
            onClickCategory?.onClick(categores[position])
        }
        holder.itemView.title_category.text = categores[position].title
        Glide.with(holder.itemView.context)
            .load(categores[position].cover)
            .into(holder.itemView.cover_category)
    }

    override fun getItemCount(): Int = categores.size

    fun setData(data: MutableList<ModelCategory>){
        categores.clear()
        categores.addAll(data)
        notifyDataSetChanged()
    }
}